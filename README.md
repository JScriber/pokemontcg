# Functionnalities

## Navigate

<img src="https://gitlab.com/JScriber/pokemontcg/-/wikis/uploads/cd56aedf60927628ce18441f20b05973/navigate.png" height="600"/>

Navigate through the pokémons!

## Search

<img src="https://gitlab.com/JScriber/pokemontcg/-/wikis/uploads/e8b25ccd85e7b9d2a3ec2f9f4c7ad19f/search.png" height="600"/>

Search with the help of the searchbar.

## Details

<img src="https://gitlab.com/JScriber/pokemontcg/-/wikis/uploads/03908307d9ce4285ffbf1e928a781509/details.png" height="600"/>

Get details on your favorite cards.

## Responsive

<img src="https://gitlab.com/JScriber/pokemontcg/-/wikis/uploads/b48b37907a2bf7b1bcdace190e15b44b/responsive_grid.png" height="650"/>
<img src="https://gitlab.com/JScriber/pokemontcg/-/wikis/uploads/2a1f4815b421c1149317a9f02f6d1ac2/responsive_grid_portrait.png" height="650"/>
