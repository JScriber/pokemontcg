import 'package:flutter/material.dart';

Color getPokemonTypeColor(String type) {
  Color color;

  switch (type) {
    case 'Fire':
      color = Colors.redAccent;
      break;
    case 'Grass':
      color = Color.fromRGBO(29, 147, 50, 1);
      break;
    case 'Water':
      color = Colors.blue;
      break;
    case 'Ice':
      color = Colors.cyanAccent;
      break;
    case 'Metal':
      color = Colors.blueGrey;
      break;
    case 'Psychic':
      color = Colors.pink;
      break;
    case 'Darkness':
      color = Colors.black;
      break;
    case 'Fairy':
      color = Color.fromRGBO(197, 24, 224, 1);
      break;
    case 'Lightning':
      color = Color.fromRGBO(224, 178, 24, 1);
      break;
    case 'Fighting':
      color = Colors.brown;
      break;
    case 'Colorless':
      color = Colors.grey;
      break;
    case 'Dragon':
      color = Colors.purple;
      break;
    default:
      color = Colors.black.withOpacity(0.5);
      break;
  }

  return color;
}
