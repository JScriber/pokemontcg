class PokemonCardAttackModel {
  List<String> cost;
  String name;
  String text;
  String damage;
  int convertedEnergyCost;

  PokemonCardAttackModel({
    this.cost,
    this.name,
    this.text,
    this.damage,
    this.convertedEnergyCost,
  });

  PokemonCardAttackModel.fromJSON(Map<String, dynamic> json) {
    cost = List<String>.from(json['cost']).toList();
    name = json['name'];
    text = json['text'];
    damage = json['damage'];
    convertedEnergyCost = json['convertedEnergyCost'];
  }
}
