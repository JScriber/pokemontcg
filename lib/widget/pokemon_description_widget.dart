import 'package:flutter/material.dart';
import 'package:flutterpokemontcg/model/pokemon_card_attack_model.dart';
import 'package:flutterpokemontcg/model/pokemon_card_model.dart';
import 'package:flutterpokemontcg/style/pokemon_type_color.dart';
import 'package:flutterpokemontcg/widget/chip_list_widget.dart';
import 'package:flutterpokemontcg/widget/chip_widget.dart';
import 'package:flutterpokemontcg/widget/label_row_widget.dart';
import 'package:flutterpokemontcg/widget/pokemon_attack_widget.dart';
import 'package:flutterpokemontcg/widget/separator_widget.dart';

class PokemonDescriptionWidget extends StatelessWidget {
  final PokemonCardModel _card;

  PokemonDescriptionWidget(this._card);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        if (_card.types != null) ...[
          LabelRowWidget(
            label: 'Types',
            value: ChipListWidget(
              _card.types.map((type) {
                return ChipItem(
                  text: type,
                  color: getPokemonTypeColor(type),
                );
              }).toList(),
            ),
          ),
        ],
        if (_card.resistances != null) ...[
          LabelRowWidget(
            label: 'Resistances',
            value: ChipListWidget(_card.resistances.map((weakness) {
              return ChipItem(
                text: '${weakness.type} ${weakness.value}',
                color: getPokemonTypeColor(weakness.type),
              );
            }).toList()),
          ),
        ],
        if (_card.weaknesses != null) ...[
          LabelRowWidget(
            label: 'Weaknesses',
            value: ChipListWidget(_card.weaknesses.map((weakness) {
              return ChipItem(
                text: '${weakness.type} ${weakness.value}',
                color: getPokemonTypeColor(weakness.type),
              );
            }).toList()),
          ),
        ],
        LabelRowWidget(
          label: 'Rarity',
          value: _buildDisplayValue(context, _card.rarity),
        ),
        if (_card.series != null) ...[
          LabelRowWidget(
            label: 'Series',
            value: _buildDisplayValue(context, _card.series),
          ),
        ],
        if (_card.cardSet != null) ...[
          LabelRowWidget(
            label: 'Set',
            value: _buildDisplayValue(context, _card.cardSet),
          ),
        ],
        if (_card.artist != null) ...[
          LabelRowWidget(
            label: 'Artist',
            value: _buildDisplayValue(context, _card.artist),
          ),
        ],
        if (_card.attacks != null && _card.attacks.length > 0) ...[
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 5.0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 23.0,
                    horizontal: 5.0,
                  ),
                  child: Text(
                    'Attacks',
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: _buildAttacks(),
                )
              ],
            ),
          ),
        ],
        Container(height: 30),
      ],
    );
  }

  Widget _buildDisplayValue(BuildContext context, String value) {
    return Text(
      value ?? '',
      style: Theme.of(context).textTheme.bodyText2,
    );
  }

  List<Widget> _buildAttacks() {
    List<Widget> widgets = [];

    for (int i = 0; i < _card.attacks.length; i++) {
      PokemonCardAttackModel attack = _card.attacks[i];
      Widget widget = Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 8.0,
        ),
        child: PokemonAttackWidget(attack),
      );

      widgets.add(widget);

      if (i != _card.attacks.length - 1) {
        widgets.add(Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 4.0,
            horizontal: 20.0,
          ),
          child: SeparatorWidget(),
        ));
      }
    }

    return widgets;
  }
}
