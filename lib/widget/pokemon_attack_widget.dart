import 'package:flutter/material.dart';
import 'package:flutterpokemontcg/model/pokemon_card_attack_model.dart';
import 'package:flutterpokemontcg/style/color.dart';
import 'package:flutterpokemontcg/style/pokemon_type_color.dart';
import 'package:flutterpokemontcg/widget/chip_list_widget.dart';
import 'package:flutterpokemontcg/widget/chip_widget.dart';

class PokemonAttackWidget extends StatelessWidget {
  final PokemonCardAttackModel attack;

  PokemonAttackWidget(this.attack);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(height: 8.0),
        Row(
          children: <Widget>[
            Image(
              image: AssetImage('assets/images/list.png'),
            ),
            Container(width: 8.0),
            Expanded(
              child: Text(
                attack.name,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.headline2,
              ),
            ),
            Text(
              attack.damage,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 16.0,
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              if (attack.text != null && attack.text.length > 0) ...[
                Text(
                  attack.text,
                  softWrap: true,
                  style: Theme.of(context).textTheme.bodyText2,
                ),
                Container(height: 8.0),
              ],
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Cost',
                    style: TextStyle(
                      fontSize: 15.0,
                      color: textColor,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 8.0, left: 15.0),
                      child: ChipListWidget(attack.cost.map((cost) {
                        return ChipItem(
                          text: cost,
                          color: getPokemonTypeColor(cost),
                        );
                      }).toList()),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
