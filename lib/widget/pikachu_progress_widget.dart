import 'package:flutter/material.dart';

class PikachuProgressWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/page_loader.gif'),
        ),
      ),
    );
  }
}
