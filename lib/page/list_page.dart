import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutterpokemontcg/model/pokemon_card_thumbnail_model.dart';
import 'package:flutterpokemontcg/service/pokemon_service_api.dart';
import 'package:flutterpokemontcg/widget/no_result_widget.dart';
import 'package:flutterpokemontcg/widget/pikachu_progress_widget.dart';
import 'package:flutterpokemontcg/widget/pokemon_card_thumbnail_widget.dart';
import 'package:flutterpokemontcg/widget/rounded_blur_widget.dart';
import 'package:flutterpokemontcg/widget/search_widget.dart';

class ListPage extends StatefulWidget {
  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> with AfterLayoutMixin {
  static const double cardSpacing = 5.0;
  static const int _initialPage = 1;

  int _currentPage = _initialPage;
  String _pattern;
  bool _isLoading = true;
  bool _isRequesting = false;
  int _cardPerRow;

  List<PokemonCardThumbnailModel> _list = [];

  @override
  void afterFirstLayout(BuildContext context) {
    _calculateCardPerRow(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  _isLoading || _list == null
                      ? Center(child: PikachuProgressWidget())
                      : _buildList(context),
                  if (_isRequesting && !_isLoading) ...[
                    Positioned(
                      left: 0,
                      right: 0,
                      bottom: 35,
                      child: Center(
                        child: RoundedBlurWidget(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                              vertical: 15.0,
                              horizontal: 20.0,
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                SizedBox(
                                  height: 25.0,
                                  width: 25.0,
                                  child: CircularProgressIndicator(
                                    strokeWidth: 3.0,
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Colors.black.withOpacity(0.7)),
                                  ),
                                ),
                                Container(width: 15.0),
                                Text(
                                  'Loading',
                                  style: TextStyle(
                                    color: Colors.black.withOpacity(0.7),
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                  Positioned(
                    top: 10,
                    left: 20,
                    right: 20,
                    child: SizedBox(
                      height: 60,
                      child: Center(
                        child: Container(
                          constraints: BoxConstraints(maxWidth: 500),
                          child: SearchWidget(
                            label: 'Search a card by its name',
                            onReset: _onReset,
                            onSearch: _onSearch,
                            initialPattern: _pattern,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildList(BuildContext context) {
    if (_list.length == 0) {
      return NoResultWidget();
    }

    _calculateCardPerRow(context);

    double realCardWidth = MediaQuery.of(context).size.width / _cardPerRow;
    double estimatedHeight = PokemonCardThumbnailWidget.thumbnailHeight + 43;

    return NotificationListener<ScrollNotification>(
      onNotification: (scrollNotification) {
        int padding = 40;

        if (scrollNotification.metrics.pixels >=
                scrollNotification.metrics.maxScrollExtent - padding &&
            !_isRequesting) {
          _onNextPage();
        }

        return true;
      },
      child: GridView.builder(
        itemCount: _list.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: _cardPerRow,
          childAspectRatio: realCardWidth / estimatedHeight,
          crossAxisSpacing: cardSpacing,
          mainAxisSpacing: cardSpacing,
        ),
        itemBuilder: (BuildContext context, int index) {
          if (index > _list.length) {
            return null;
          }

          return PokemonCardThumbnailWidget(
            thumbnail: _list[index],
            onPressed: (thumbnail) {
              Navigator.pushNamed(context, 'detail', arguments: thumbnail);
            },
          );
        },
        padding: EdgeInsets.only(
          top: 80,
          bottom: 100,
        ),
      ),
    );
  }

  _onSearch(String pattern) {
    if (_pattern != pattern) {
      setState(() {
        _currentPage = _initialPage;
        _pattern = pattern;
        _list = [];
      });

      _getCards(loading: true);
    }
  }

  _onReset() {
    setState(() {
      _currentPage = _initialPage;
      _pattern = null;
      _list = [];
    });

    _getCards(loading: true);
  }

  _onNextPage() {
    setState(() => _currentPage++);
    _getCards(loading: false);
  }

  _getCards({bool loading}) async {
    setState(() {
      if (loading) {
        _isLoading = true;
      }

      _isRequesting = true;
    });

    List<PokemonCardThumbnailModel> list = await PokemonServiceApi.getCards(
      page: _currentPage,
      contain: _pattern,
      size: _cardPerRow * 5,
    );

    setState(() {
      _list = [..._list, ...list];
      _isRequesting = false;

      if (loading) {
        _isLoading = false;
      }
    });
  }

  void _calculateCardPerRow(BuildContext context) {
    double cardWidth = PokemonCardThumbnailWidget.thumbnailWidth +
        (cardSpacing * 2) +
        (PokemonCardThumbnailWidget.padding * 2);
    int cardPerRow = (MediaQuery.of(context).size.width / cardWidth).round();

    if (cardPerRow != _cardPerRow) {
      setState(() {
        _cardPerRow = cardPerRow;

        _currentPage = _initialPage;
        _list = [];
      });

      _getCards(loading: true);
    }
  }
}
