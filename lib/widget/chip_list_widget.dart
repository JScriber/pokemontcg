import 'package:flutter/material.dart';
import 'package:flutterpokemontcg/widget/chip_widget.dart';

class ChipListWidget extends StatelessWidget {
  final List<ChipItem> items;

  ChipListWidget(this.items);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 8.0,
      runSpacing: 8.0,
      children: items.map((item) => ChipWidget(item)).toList(),
    );
  }
}
