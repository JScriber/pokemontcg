import 'package:flutter/material.dart';
import 'package:flutterpokemontcg/style/color.dart';

ThemeData appTheme = ThemeData(
  textTheme: TextTheme(
    headline1: TextStyle(
      color: textColor,
      fontWeight: FontWeight.w800,
      fontSize: 28.0,
    ),
    headline2: TextStyle(
      color: textColor,
      fontWeight: FontWeight.w700,
      fontSize: 17.0,
    ),
    bodyText2: TextStyle(
      color: textColor.withOpacity(0.65),
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
    ),
  ),
);
