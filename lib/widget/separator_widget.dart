import 'package:flutter/material.dart';

class SeparatorWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 1,
      color: Color.fromRGBO(200, 200, 200, 1),
    );
  }
}
