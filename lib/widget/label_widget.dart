import 'package:flutter/material.dart';

class LabelWidget extends StatelessWidget {
  final String text;

  LabelWidget(this.text);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 8.0,
        horizontal: 15.0,
      ),
      child: Text(
        this.text,
        style: Theme.of(context).textTheme.headline2,
      ),
    );
  }
}
