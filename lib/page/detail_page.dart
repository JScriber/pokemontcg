import 'dart:ui';

import 'package:after_layout/after_layout.dart';
import 'package:color_thief_flutter/color_thief_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterpokemontcg/model/pokemon_card_model.dart';
import 'package:flutterpokemontcg/model/pokemon_card_thumbnail_model.dart';
import 'package:flutterpokemontcg/service/pokemon_service_api.dart';
import 'package:flutterpokemontcg/widget/pikachu_progress_widget.dart';
import 'package:flutterpokemontcg/widget/pokemon_description_widget.dart';
import 'package:tinycolor/tinycolor.dart';

class DetailPage extends StatefulWidget {
  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> with AfterLayoutMixin {
  PokemonCardModel _card;
  bool _isLoading = true;
  Color _color = Colors.transparent;

  @override
  void afterFirstLayout(BuildContext context) {
    PokemonCardThumbnailModel thumbnail =
        ModalRoute.of(context).settings.arguments;

    if (thumbnail != null && thumbnail.id != null) {
      _getCard(thumbnail.id);
    } else {
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _isLoading || _card == null
          ? Center(child: PikachuProgressWidget())
          : _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return _buildDecorations(
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // Replaces SafeArea to get the background displayed.
          Container(height: MediaQuery.of(context).padding.top),
          _buildAppbar(context),
          Expanded(
            child: ListView(
              children: <Widget>[
                Stack(
                  fit: StackFit.loose,
                  overflow: Overflow.visible,
                  children: <Widget>[
                    Positioned(
                      top: 0,
                      left: 0,
                      right: 0,
                      bottom: -1,
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          height: 130,
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(245, 245, 245, 1),
                            borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(45),
                              topRight: const Radius.circular(45),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 3.0,
                        bottom: 40.0,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: 440,
                            margin: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  spreadRadius: 1,
                                  offset: Offset(0, 4),
                                  blurRadius: 30,
                                  color: _color,
                                ),
                              ],
                            ),
                            child: Image.network(_card.picture),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(245, 245, 245, 1),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20.0,
                    ),
                    child: PokemonDescriptionWidget(_card),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDecorations(Widget content) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(_card.picture),
              fit: BoxFit.cover,
            ),
          ),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 20, sigmaY: 20),
            child: Container(
              color: _color.withOpacity(0.6),
            ),
          ),
        ),
        content,
      ],
    );
  }

  Widget _buildAppbar(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 4.0,
        horizontal: 12.0,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 55,
            width: 55,
            child: FlatButton(
              child: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              onPressed: () => Navigator.pop(context),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50),
              ),
            ),
          ),
          Container(
            width: 10,
          ),
          Expanded(
            child: Text(
              _card.name,
              style: TextStyle(
                fontSize: 21,
                fontWeight: FontWeight.w700,
                color: Colors.white,
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          if (_card.nationalPokedexNumber != null) ...[
            Text(
              '#${_card.nationalPokedexNumber}',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 17,
                color: Colors.white.withOpacity(0.8),
              ),
            ),
          ],
          Container(width: 10),
        ],
      ),
    );
  }

  void _getCard(String id) async {
    setState(() {
      _isLoading = true;
    });

    PokemonCardModel card = await PokemonServiceApi.getCard(id);
    Color imageColor = await _extractMainColor(card.picture);

    setState(() {
      _isLoading = false;
      _card = card;

      _color = TinyColor(imageColor).darken(25).color;
    });
  }

  Future<Color> _extractMainColor(String imageUrl) async {
    List<int> rgbProfile = await getColorFromUrl(imageUrl);

    return Color.fromRGBO(rgbProfile[0], rgbProfile[1], rgbProfile[2], 1);
  }
}
