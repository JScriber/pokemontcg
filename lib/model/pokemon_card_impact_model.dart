class PokemonCardImpactModel {
  String type;
  String value;

  PokemonCardImpactModel({
    this.type,
    this.value,
  });

  PokemonCardImpactModel.fromJSON(Map<String, dynamic> json) {
    type = json['type'];
    value = json['value'];
  }
}
