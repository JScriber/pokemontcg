import 'dart:ui';

import 'package:flutter/material.dart';

class RoundedBlurWidget extends StatelessWidget {
  final Widget child;
  final double radius;
  final double blur;
  final Color backgroundColor;

  RoundedBlurWidget({
    @required this.child,
    this.radius = 30.0,
    this.blur = 25.0,
    this.backgroundColor = const Color.fromRGBO(240, 240, 240, .6),
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: blur, sigmaY: blur),
        child: Container(
          decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: BorderRadius.all(Radius.circular(radius)),
          ),
          child: child,
        ),
      ),
    );
  }
}
