import 'package:flutterpokemontcg/model/pokemon_card_attack_model.dart';
import 'package:flutterpokemontcg/model/pokemon_card_impact_model.dart';

class PokemonCardModel {
  String id;
  String picture;
  String name;
  String rarity;
  String series;
  String cardSet;
  String artist;
  int nationalPokedexNumber;
  List<String> types;
  int hp;
  List<PokemonCardAttackModel> attacks;
  List<PokemonCardImpactModel> weaknesses;
  List<PokemonCardImpactModel> resistances;

  PokemonCardModel({
    this.id,
    this.picture,
    this.name,
    this.types,
    this.hp,
    this.attacks,
  });

  PokemonCardModel.fromJSON(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    picture = json['imageUrlHiRes'];
    rarity = json['rarity'];
    series = json['series'];
    cardSet = json['set'];
    artist = json['artist'];

    if (json.containsKey('nationalPokedexNumber')) {
      nationalPokedexNumber = json['nationalPokedexNumber'];
    }

    if (json.containsKey('hp')) {
      hp = int.tryParse(json['hp']);
    }

    if (json.containsKey('types')) {
      types = List<String>.from(json['types']).toList();
    }

    if (json.containsKey('attacks')) {
      attacks = List.from(json['attacks'])
          .map((e) => PokemonCardAttackModel.fromJSON(e))
          .toList();
    }

    if (json.containsKey('weaknesses')) {
      weaknesses = List.from(json['weaknesses'])
          .map((e) => PokemonCardImpactModel.fromJSON(e))
          .toList();
    }

    if (json.containsKey('resistances')) {
      resistances = List.from(json['resistances'])
          .map((e) => PokemonCardImpactModel.fromJSON(e))
          .toList();
    }
  }
}
