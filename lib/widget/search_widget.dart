import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutterpokemontcg/widget/rounded_blur_widget.dart';

class SearchWidget extends StatefulWidget {
  final String label;
  final Function onSearch;
  final Function onReset;
  final String initialPattern;

  SearchWidget({this.label, this.onSearch, this.onReset, this.initialPattern});

  @override
  _SearchWidgetState createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  final TextEditingController _controller = TextEditingController();

  bool _showClearButton = false;

  @override
  void initState() {
    _controller.text = widget.initialPattern;

    if (widget.initialPattern != null) {
      setState(() => _showClearButton = true);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RoundedBlurWidget(
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 8.0,
        ),
        child: Row(
          children: <Widget>[
            _buildButton(
              behaviour: _search,
              icon: Icons.search,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 8.0,
                ),
                child: _buildTextField(),
              ),
            ),
            if (_showClearButton) ...[
              _buildButton(
                behaviour: _reset,
                icon: Icons.clear,
              ),
            ]
          ],
        ),
      ),
    );
  }

  Widget _buildButton({Function behaviour, IconData icon}) {
    return SizedBox(
      height: 60,
      width: 60,
      child: FlatButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
        onPressed: behaviour,
        child: Icon(icon),
      ),
    );
  }

  Widget _buildTextField() {
    return TextFormField(
      autofocus: false,
      onFieldSubmitted: (_) => _search(),
      decoration: InputDecoration(
        hintText: widget.label,
        border: InputBorder.none,
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        disabledBorder: InputBorder.none,
      ),
      style: TextStyle(
        color: Colors.black,
        fontSize: 17,
      ),
      controller: _controller,
      onChanged: (text) {
        if (text.length > 0 && !_showClearButton) {
          setState(() => _showClearButton = true);
        } else {
          if (text.length == 0 && _showClearButton) {
            setState(() => _showClearButton = false);
          }
        }
      },
    );
  }

  _search() {
    return widget.onSearch(_controller.text);
  }

  _reset() {
    _controller.clear();
    _showClearButton = false;
    widget.onReset();
  }
}
