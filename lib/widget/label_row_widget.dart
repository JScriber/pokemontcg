import 'package:flutter/material.dart';
import 'package:flutterpokemontcg/widget/label_widget.dart';

class LabelRowWidget extends StatelessWidget {
  final String label;
  final Widget value;

  LabelRowWidget({this.label, this.value});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 5.0,
      ),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 140,
            child: LabelWidget(label),
          ),
          Expanded(
            child: value,
          ),
        ],
      ),
    );
  }
}
