import 'package:flutter/material.dart';
import 'package:flutterpokemontcg/page/detail_page.dart';
import 'package:flutterpokemontcg/page/list_page.dart';

Map<String, WidgetBuilder> routes = {
  'list': (_) => ListPage(),
  'detail': (_) => DetailPage()
};
