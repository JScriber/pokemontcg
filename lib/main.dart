import 'package:flutter/material.dart';
import 'package:flutterpokemontcg/route/routes.dart';
import 'package:flutterpokemontcg/style/theme.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: appTheme,
      debugShowCheckedModeBanner: false,
      initialRoute: 'list',
      routes: routes,
      // theme: appTheme,
    );
  }
}
