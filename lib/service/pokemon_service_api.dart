import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutterpokemontcg/model/pokemon_card_model.dart';
import 'package:flutterpokemontcg/model/pokemon_card_thumbnail_model.dart';
import 'package:http/http.dart' as http;

class PokemonServiceApi {
  static final _baseURL = 'api.pokemontcg.io';

  static Future<List<PokemonCardThumbnailModel>> getCards({
    @required int page,
    String contain,
    int size = 10,
  }) async {
    Uri uri = Uri.https(_baseURL, '/v1/cards', {
      'page': page.toString(),
      'pageSize': size.toString(),
      'name': contain,
    });

    print('Get page $page of size $size');

    http.Response response = await http.get(uri);
    Map<String, dynamic> json = jsonDecode(response.body);

    return List.from(json['cards'])
        .map((e) => PokemonCardThumbnailModel.fromJSON(e))
        .toList();
  }

  static Future<PokemonCardModel> getCard(String id) async {
    Uri uri = Uri.https(_baseURL, '/v1/cards/$id');

    http.Response response = await http.get(uri);

    return PokemonCardModel.fromJSON(jsonDecode(response.body)['card']);
  }
}
