import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutterpokemontcg/model/pokemon_card_thumbnail_model.dart';
import 'package:flutterpokemontcg/style/color.dart';

class PokemonCardThumbnailWidget extends StatelessWidget {
  static const double thumbnailWidth = 210;
  static const double thumbnailHeight = 1.3 * thumbnailWidth;
  static const double padding = 5.0;

  final PokemonCardThumbnailModel thumbnail;
  final Function onPressed;

  PokemonCardThumbnailWidget({this.thumbnail, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () => onPressed(thumbnail),
        child: Padding(
          padding: const EdgeInsets.all(padding),
          child: Column(
            children: <Widget>[
              SizedBox(
                width: thumbnailWidth,
                height: thumbnailHeight,
                // TransitionToImage is used instead of FadeInImage because the
                // imageErrorBuilder callback parameter makes the app crash when
                // the widget is disposed.
                child: TransitionToImage(
                  image: AdvancedNetworkImage(
                    thumbnail.picture,
                    useDiskCache: false,
                    retryLimit: 2,
                    fallbackAssetImage: 'assets/images/link_broken.png',
                  ),
                  loadingWidget: Image(
                    image: AssetImage('assets/images/loader.gif'),
                  ),
                  transitionType: TransitionType.fade,
                  curve: Curves.easeIn,
                  duration: Duration(milliseconds: 700),
                ),
              ),
              Container(height: 5),
              Text(
                thumbnail.name,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w700,
                  color: textColor,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
