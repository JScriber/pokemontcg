class PokemonCardThumbnailModel {
  String id;
  String picture;
  String name;

  PokemonCardThumbnailModel({this.id, this.picture, this.name});

  PokemonCardThumbnailModel.fromJSON(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    picture = json['imageUrl'];
  }
}
